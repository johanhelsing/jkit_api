use bevy::prelude::*;

#[derive(Reflect)]
pub struct JKitApiSettings {
    pub endpoint: String,
    pub game: String,
    pub game_version: String,
}
