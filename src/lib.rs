mod api;
mod gql;
mod jwt;
mod resources;
mod systems;

use bevy::prelude::*;
use systems::*;

pub use api::{LoginError, Session};
pub use resources::JKitApiSettings;

#[derive(Default)]
pub struct JKitApiPlugin;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum LoginState {
    Login,
    LoggedIn,
}

pub struct SessionUpdatedEvent;

impl Plugin for JKitApiPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(login)
            .add_event::<SessionUpdatedEvent>()
            .add_system(check_login_task)
            .add_system(update_login_state)
            .add_system_set(SystemSet::on_update(LoginState::LoggedIn).with_system(refresh_token))
            .add_state(LoginState::Login);
    }
}
