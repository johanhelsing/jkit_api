use crate::gql::queries::{self, LoginArguments, LoginRequest};
use crate::jwt::{self, Jwt};
use bevy::prelude::{error, info, warn};
use cynic::{http::SurfExt, MutationBuilder};

#[derive(Debug, Clone)]
pub struct AuthInfo {
    pub game: String,
    pub game_version: Option<String>,
    pub endpoint: String,
    pub device_id: Option<String>,
    /// For when the game is hosted on the web, this is the url in the url-bar
    pub url: Option<String>,
}

#[derive(Debug, Clone)]
pub struct Session {
    pub auth_info: AuthInfo,
    pub token: String,
    pub refresh_token: String,
}

impl Default for AuthInfo {
    fn default() -> Self {
        #[cfg(target_arch = "wasm32")]
        let url = web_sys::window().unwrap().location().href().ok();
        #[cfg(not(target_arch = "wasm32"))]
        let url = None;

        Self {
            game: env!("CARGO_PKG_NAME").to_string(),
            endpoint: "http://localhost:8080/v1/graphql".to_string(),
            device_id: None,
            game_version: None,
            url,
        }
    }
}

impl AuthInfo {
    async fn fresh_login(self) -> Result<Session, LoginError> {
        let response = {
            let op = queries::Login::build(&LoginArguments {
                req: LoginRequest {
                    device_id: self.device_id.to_owned().map(|id| id.into()),
                    game: Some(self.game.to_owned()),
                    game_version: self.game_version.to_owned(),
                    platform: Some(std::env::consts::OS.into()),
                    url: self.url.to_owned(),
                },
            });
            let endpoint = self.endpoint.to_owned();
            surf::post(&endpoint).run_graphql(op)
        }
        .await?;

        if let Some(errors) = response.errors {
            Err(LoginError::GraphQl(errors))
        } else if let Some(data) = response.data {
            let auth = data.auth;
            let token = auth.access_token;
            let jwt = Jwt::decode(&token)?;
            info!("Logged in: {jwt:?}");
            Ok(Session {
                refresh_token: auth.refresh_token,
                auth_info: self.clone(),
                token,
            })
        } else {
            Err(LoginError::EmptyResponse)
        }
        // todo set tokens
    }

    async fn refresh_token_login(self, refresh_token: String) -> Result<Session, LoginError> {
        info!("Using stored refresh token for login");

        let response = {
            let op = queries::RefreshToken::build(&queries::RefreshTokenArguments {
                req: queries::RefreshRequest {
                    refresh_token,
                    device_id: self.device_id.to_owned().map(|id| id.into()),
                    game: Some(self.game.to_owned()),
                    game_version: self.game_version.to_owned(),
                    grant_type: "".to_string(),
                    platform: Some(std::env::consts::OS.into()),
                    url: None,
                },
            });
            let endpoint = self.endpoint.to_owned();
            surf::post(&endpoint).run_graphql(op)
        }
        .await?;

        // todo: could dedup this
        if let Some(errors) = response.errors {
            Err(LoginError::GraphQl(errors))
        } else if let Some(data) = response.data {
            let auth = data.refresh_token;
            let token = auth.access_token;
            let jwt = Jwt::decode(&token)?;
            info!("Logged in with refresh token: {jwt:?}");
            Ok(Session {
                auth_info: self.clone(),
                token,
                refresh_token: auth.refresh_token,
            })
        } else {
            Err(LoginError::EmptyResponse)
        }

        // todo set tokens
    }

    pub async fn login(self, refresh_token: Option<String>) -> Result<Session, LoginError> {
        info!("Logging in to {}", self.endpoint);
        if let Some(refresh_token) = refresh_token {
            match self.clone().refresh_token_login(refresh_token).await {
                Err(e) => warn!("Refresh token login failed: {e}"),
                res => return res,
            }
        }
        // if there is no refresh token or refresh token login failed, do a fresh login
        self.fresh_login().await
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LoginError {
    #[error("Graphql error: {0:?}")]
    GraphQl(Vec<cynic::GraphQlError>),
    #[error("Jwt decode error")]
    JwtDecode(#[from] jwt::DecodeError),
    // I don't think this will ever happen...
    #[error("GraphQl response didn't contain any data")]
    EmptyResponse,
    #[error("Http error")]
    Http(surf::Error),
}

impl From<surf::Error> for LoginError {
    fn from(err: surf::Error) -> Self {
        LoginError::Http(err)
    }
}

impl Session {
    pub async fn refresh(self) -> Result<Session, LoginError> {
        self.auth_info.login(Some(self.refresh_token)).await
    }
}
