use crate::api::{AuthInfo, LoginError, Session};

use super::*;
use bevy::prelude::*;
use bevy::tasks::IoTaskPool;
use bevy_pkv::PkvStore;
use futures_channel::oneshot;
use uuid::Uuid;

pub fn login(settings: Res<JKitApiSettings>, mut commands: Commands, mut pkv: ResMut<PkvStore>) {
    let device_id = match pkv.get("jkit_api_device_id") {
        Ok(id) => id,
        Err(bevy_pkv::GetError::NotFound) => {
            let id = Uuid::new_v4().to_string();
            if let Err(e) = pkv.set("jkit_api_device_id", &id) {
                error!("Failed to persist client id: {e}");
            }
            id
        }
        Err(e) => {
            error!("Failed to get client id: {e}");
            return;
        }
    };

    let refresh_token = pkv.get("jkit_api_refresh_token").ok();
    let client = AuthInfo {
        endpoint: settings.endpoint.to_owned(),
        game: settings.game.to_owned(),
        game_version: Some(settings.game_version.to_owned()),
        device_id: Some(device_id),
        ..default()
    };

    let (sender, receiver) = oneshot::channel();
    let task_pool = IoTaskPool::get();
    task_pool
        .spawn(async move {
            let _ = sender.send(client.login(refresh_token).await);
        })
        .detach();

    commands.spawn().insert(LoginTask(receiver));
}

#[derive(Component, Deref, DerefMut)]
pub(crate) struct LoginTask(oneshot::Receiver<Result<Session, LoginError>>);

pub(crate) fn check_login_task(
    mut query: Query<(Entity, &mut LoginTask)>,
    mut commands: Commands,
    mut pkv: ResMut<PkvStore>,
    mut session_updated_events: EventWriter<SessionUpdatedEvent>,
    mut session_res: Option<ResMut<Session>>,
) {
    for (entity, mut task) in query.iter_mut() {
        if let Some(login_result) = task.0.try_recv().expect("sender dropped") {
            match login_result {
                Ok(session) => {
                    if let Err(e) = pkv.set("jkit_api_refresh_token", &session.refresh_token) {
                        error!("Failed to store refresh token: {e}");
                    } else {
                        info!("Stored refresh token for later");
                    }
                    if let Some(session_res) = session_res.as_mut() {
                        **session_res = session;
                    } else {
                        commands.insert_resource(session);
                    }
                    session_updated_events.send(SessionUpdatedEvent);
                }
                Err(e) => error!("Login failed: {e}"),
            }
            commands.entity(entity).remove::<LoginTask>();
        }
    }
}

pub fn update_login_state(
    mut login_state: ResMut<State<LoginState>>,
    session: Option<Res<Session>>,
) {
    if login_state.current() == &LoginState::Login && session.is_some() {
        login_state.set(LoginState::LoggedIn).unwrap();
    }
}

pub fn refresh_token(
    mut commands: Commands,
    session: Res<Session>,
    mut timer: Local<Timer>,
    time: Res<Time>,
) {
    let refresh_interval = 10. * 60.;
    if timer.duration().is_zero() {
        // initialize timer
        *timer = Timer::from_seconds(refresh_interval, true);
    }
    if timer.tick(time.delta()).just_finished() {
        let (sender, receiver) = oneshot::channel();
        let session = session.clone();
        let task_pool = IoTaskPool::get();
        task_pool
            .spawn(async move {
                let _ = sender.send(session.refresh().await);
            })
            .detach();
        commands.spawn().insert(LoginTask(receiver));
    }
}
