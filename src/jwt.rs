//! could just use jwt library for this, however they're all kind of bloated...

use chrono::{serde::ts_seconds_option, DateTime, Utc};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Jwt {
    /// Issuer, string or URI
    pub iss: Option<String>,
    /// Subject, string or URI
    pub sub: Option<String>,
    /// Issued at
    #[serde(with = "ts_seconds_option")]
    pub iat: Option<DateTime<Utc>>,
    /// Expiration time
    #[serde(default)]
    #[serde(with = "ts_seconds_option")]
    pub exp: Option<DateTime<Utc>>,
}

#[derive(thiserror::Error, Debug)]
pub enum DecodeError {
    #[error("Couldn't split token into parts")]
    Split,
    #[error("Couldn't decode payload base64")]
    Base64(#[from] base64::DecodeError),
    #[error("Couldn't parse string as utf-8")]
    Utf8(#[from] std::str::Utf8Error),
    #[error("Couldn't parse payload json")]
    SerdeJson(#[from] serde_json::Error),
}

impl Jwt {
    pub fn decode<T: AsRef<str>>(token: T) -> Result<Self, DecodeError> {
        let mut parts = token.as_ref().split('.');
        let payload = parts.nth(1).ok_or(DecodeError::Split)?;
        let bytes = base64::decode(payload)?;
        let json = std::str::from_utf8(&bytes)?;
        Ok(serde_json::from_str(json)?)
    }

    // /// Returns false if the jwt doesn't contain an exp field
    // pub fn expired(&self) -> bool {
    //     self.exp.map(|exp| Utc::now() > exp).unwrap_or(false)
    // }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn decode() {
        let jwt = Jwt::decode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3NTllMjBjZi03YzUxLTQyYjctYTNiMC1hMjEyYWM3OGFiNDUiLCJzZXNzaW9uIjoiOTkwOTNjMjgtNmNhYS00NzMwLWExY2ItMWQ4OGJjNjhhYWZkIiwiaWF0IjoxNjQ3MzM1MjY0LCJleHAiOjE2NDc0MjE2NjQsImh0dHBzOi8vaGFzdXJhLmlvL2p3dC9jbGFpbXMiOnsieC1oYXN1cmEtZGVmYXVsdC1yb2xlIjoidXNlciIsIngtaGFzdXJhLWFsbG93ZWQtcm9sZXMiOlsidXNlciJdLCJ4LWhhc3VyYS11c2VyLWlkIjoiNzU5ZTIwY2YtN2M1MS00MmI3LWEzYjAtYTIxMmFjNzhhYjQ1IiwieC1oYXN1cmEtZ2FtZSI6ImNteWsifX0.tU_Dt6XEH6o-yLPEC14uMmq3If8luOCsI0B_c-4MvYE").unwrap();
        insta::assert_debug_snapshot!(jwt);
        // assert!(jwt.expired());
    }
}
