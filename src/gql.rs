//! This is mainly generated from graphql
//! Some edits to make it work with 1.0
//! Tests are written manually

#[cynic::schema_for_derives(file = "schemas/schema.graphql", module = "schema")]

pub mod queries {
    use super::schema;
    use derive_more::From;

    #[derive(cynic::FragmentArguments, Debug)]
    pub struct LoginArguments {
        pub req: LoginRequest,
    }

    #[derive(cynic::FragmentArguments, Debug)]
    pub struct RefreshTokenArguments {
        pub req: RefreshRequest,
    }

    #[derive(cynic::QueryFragment, Debug)]
    #[cynic(
        graphql_type = "mutation_root",
        argument_struct = "RefreshTokenArguments"
    )]
    pub struct RefreshToken {
        #[arguments(req = &args.req)]
        pub refresh_token: LoginResponse,
    }

    #[derive(cynic::QueryFragment, Debug)]
    #[cynic(graphql_type = "mutation_root", argument_struct = "LoginArguments")]
    pub struct Login {
        #[arguments(req = &args.req)]
        pub auth: LoginResponse,
    }

    #[derive(cynic::QueryFragment, Debug)]
    pub struct LoginResponse {
        pub access_token: String,
        pub expires_in: i32,
        pub refresh_token: String,
        pub token_type: String,
    }

    #[derive(cynic::InputObject, Debug)]
    pub struct RefreshRequest {
        pub device_id: Option<Uuid>,
        pub game: Option<String>,
        pub game_version: Option<String>,
        pub grant_type: String,
        pub platform: Option<String>,
        pub refresh_token: String,
        pub url: Option<String>,
    }

    #[derive(cynic::InputObject, Debug, Clone)]
    pub struct LoginRequest {
        pub device_id: Option<Uuid>,
        pub game: Option<String>,
        pub game_version: Option<String>,
        pub platform: Option<String>,
        pub url: Option<String>,
    }

    #[derive(cynic::Scalar, Debug, Clone, From)]
    pub struct Uuid(pub String);
}

mod schema {
    cynic::use_schema!("schemas/schema.graphql");
}

#[cfg(test)]
mod tests {
    use super::queries::*;
    use cynic::MutationBuilder;
    use insta::assert_snapshot;

    #[test]
    fn login() {
        let op = Login::build(&LoginArguments {
            req: LoginRequest {
                device_id: None,
                game: Some("test_game".to_string()),
                game_version: None,
                platform: None,
                url: None,
            },
        });
        assert_snapshot!(op.query);
    }
}
